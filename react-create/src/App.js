import { Main } from "./Pages/index";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <Main />
    </div>
  );
}

export default App;
