import tmb1 from '../../Image/tmb1.jpg'
import tmb2 from '../../Image/tmb2.jpg'
import tmb3 from '../../Image/tmb3.jpg'



export const Portofolio = () => {
    return (
        <div>
            <section class="pt-5 text-center container">
                <div class="row py-lg-3">
                    <div class="col-lg-6 col-md-8 mx-auto">
                        <h1 class="fw-dark">My Choir Show</h1>
                    </div>
                </div>
            </section>
            <div class="album py-5">
                <div class="container">
                    <div class="row row-cols-1 row-cols-sm-1 row-cols-md-3 g-3">
                        <div class="col">
                            <div class="card shadow-sm">
                                <div class="text-center">
                                    <img src={tmb1} class="rounded img-fluid w-100"
                                        alt="tmb1"></img>
                                </div>

                                <div class="card-body">
                                    <p class="card-text">This is my first project in music
                                        editing for choir PSM Voca Al Kindi
                                        the name of the Song is Road Home.</p>
                                    <div
                                        class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-outline-dark btn-outline-secondary"
                                                href="https://www.youtube.com/watch?v=c-F7ypdXUwQ"
                                                role="button">Watch</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card shadow-sm">
                                <div class="text-center">
                                    <img src={tmb2} class="rounded img-fluid w-100"
                                        alt="tmb2"></img>
                                </div>

                                <div class="card-body">
                                    <p class="card-text">This is my first project in music
                                        editing for choir PSM Voca Al Kindi
                                        the name of the Song is Road Home.</p>
                                    <div
                                        class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-outline-dark btn-outline-secondary"
                                                href="https://www.youtube.com/watch?v=XjHgppaGFPg"
                                                role="button">Watch</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card shadow-sm">
                                <div class="text-center">
                                    <img src={tmb3} class="rounded img-fluid w-100"
                                        alt="tmb3"></img>
                                </div>

                                <div class="card-body">
                                    <p class="card-text">This is my first project in music
                                        editing for choir PSM Voca Al Kindi
                                        the name of the Song is Road Home.</p>
                                    <div
                                        class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-outline-dark btn-outline-secondary"
                                                href="https://www.youtube.com/watch?v=ExtehIq7-rI"
                                                role="button">Watch</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}