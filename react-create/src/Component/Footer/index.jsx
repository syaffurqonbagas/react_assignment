export const Footer = () => {
    return (
        <div>
            <footer>
                <div class="container text-center py-3">
                    <p> ©2021 Bagas Syaffurqon <span></span><svg
                        xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red"
                        class="bi bi-heart-fill" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z">
                        </path>
                    </svg>
                    </p>
                </div>
            </footer>
            <script
                src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                crossorigin="anonymous">
            </script>
        </div>
    )
}
