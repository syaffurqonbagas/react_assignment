export const Form = () => {
    return (
        <div>
            <div class="bg-dark">
                <div class="container bg-dark" id="contact">
                    <div class="row p-4">
                        <h1 class="text-center py-5 text-light">Contact Me</h1>
                        <div class="col-12 col-lg-6 mx-auto">
                            <form>
                                <div class="mb-3">
                                    <label for="exampleInputEmail1"
                                        class="form-label text-light">Email address</label>
                                    <input type="email" class="form-control"
                                        id="exampleInputEmail1" aria-describedby="emailHelp"></input>
                                    <div id="emailHelp" class="form-text">We'll never share your
                                        email with anyone else.</div>
                                </div>
                                <div class="mb-3">
                                    <label for="exampleFormControlTextarea1"
                                        class="form-label text-light">Message</label>
                                    <textarea class="form-control"
                                        id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
