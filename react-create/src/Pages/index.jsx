import React, { Fragment } from "react"

import '../Pages/main/style.css'
import { ControlledCarousel } from "../../src/Component/Hero/index"
import { Navbaratas } from "../Component/Navbar"
import { Portofolio } from "../Component/Portofolio"
import { Form } from "../Component/Form"
import { Footer } from "../Component/Footer"



export const Main = () => {
    return (
        <Fragment>
            <Navbaratas />
            <ControlledCarousel />
            <Portofolio />
            <Form />
            <Footer />
        </Fragment>

    )
}
